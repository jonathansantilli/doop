This directory contains a test program that uses Doop as a library.

Quick setup:

Step 1. Build the example program:
```
gradle build
```

Step 2. Set environment variable `DOOP_HOME` to doop-home directory (see message printed by previous step).

Step 3. Run the example program via Gradle:
```
gradle run
```
